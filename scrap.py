from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager
import os
import requests
import json
import time
import sys
from selenium.common.exceptions import NoSuchElementException, TimeoutException
from vinted_scraper import VintedScraper

def scroll_to_bottom(driver):
    last_height = driver.execute_script("return document.body.scrollHeight")
    while True:
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        time.sleep(2)  # Attendre que les éléments se chargent
        new_height = driver.execute_script("return document.body.scrollHeight")
        if new_height == last_height:
            break
        last_height = new_height

def wait_and_click_accept_button(driver):
    try:
        accept_button = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.ID, "onetrust-accept-btn-handler"))
        )
        accept_button.click()
        print("Clicked on the accept button.")
    except TimeoutException:
        print("Accept button not found within 10 seconds.")

def download_pictures(item_details, directory):
    photo_urls = [photo.url for photo in item_details.photos]
    for idx, url in enumerate(photo_urls):
        response = requests.get(url)
        if response.status_code == 200:
            with open(f"{directory}/photo_{idx + 1}.jpg", 'wb') as photo_file:
                photo_file.write(response.content)

def details_json(item_details, directory):
    item_dict = serialize(item_details)
    json_path = f"{directory}/details.json"
    with open(json_path, 'w') as json_file:
        json_file.write(json.dumps(item_dict, indent=4, ensure_ascii=False))
    post_path = f"{directory}/post.txt"
    with open(post_path, 'w', encoding='utf-8') as post_file:
        post_file.write(f"Titre de l'annonce : {item_dict.get('title', 'N/A')}\n")
        post_file.write(f"Categorie : {item_dict.get('catalog_branch_title', 'N/A')}\n")
        post_file.write(f"Prix : {item_dict.get('price', 'N/A')} €\n")
        post_file.write("\nDescription :\n")
        post_file.write(f"- Marque : {item_dict.get('brand', {}).get('title', 'N/A')}\n")
        post_file.write(f"- Taille : {item_dict.get('size', 'N/A')}\n")
        post_file.write(f"- Etat : {item_dict.get('status', 'N/A')}\n")
        post_file.write(f"- Couleur principale : {item_dict.get('color1', 'N/A')}\n")
        post_file.write(f"\n{item_dict.get('description', 'N/A')}")

def get_links(driver):
    try:
        all_elements = WebDriverWait(driver, 20).until(
            EC.presence_of_all_elements_located((By.CSS_SELECTOR, '.new-item-box__overlay.new-item-box__overlay--clickable'))
        )
        return [element.get_attribute("href") for element in all_elements]
    except TimeoutException:
        print("Les éléments cliquables n'ont pas été trouvés dans le temps imparti.")
        return []

def serialize(obj):
    if hasattr(obj, "__dict__"):
        return {key: serialize(value) for key, value in obj.__dict__.items()}
    elif isinstance(obj, list):
        return [serialize(item) for item in obj]
    elif isinstance(obj, dict):
        return {key: serialize(value) for key, value in obj.items()}
    else:
        return str(obj)

def main(member_id):
    chrome_options = Options()
#     chrome_options.add_argument("--headless")
#     chrome_options.add_argument("--disable-gpu")
    driver = webdriver.Chrome(options=chrome_options)

    scraper = VintedScraper("https://www.vinted.fr")
    driver.get(f"https://www.vinted.fr/member/{member_id}")

    wait_and_click_accept_button(driver)
    scroll_to_bottom(driver)
    links = get_links(driver)
    processed_items = 0

    for link in links:
        processed_items += 1
        item_id = link.split("/")[-1]
        item_details = scraper.item(item_id)
        item_dict = serialize(item_details)
        directory = f"./scrapped/{member_id}/{item_dict.get('price', 'N/A')}€-{item_id}"

        if not os.path.exists(directory):
            print(f"Scraping article {processed_items}/{len(links)} ({item_id})")
            os.makedirs(directory, exist_ok=True)
            details_json(item_details, directory)
            download_pictures(item_details, directory)

if __name__ == "__main__":
    member_id = sys.argv[1] if len(sys.argv) > 1 else 'default_member_id'
    main(member_id)
