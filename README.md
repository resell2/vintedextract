# Extacteur d'annonces Vinted

Ce script ne requiert aucune information confidentielle et ne fait que lire les annonces de Vinted.
Pas besoin d'identifiants, ni mot de passe, ni de token, ni de quoi que ce soit d'autre.

Je recommande de basculer en 4g / 5g pour éviter que le compte qui votre IP domestique soit bloqué (c'est jamais arrivé,
mais on en sait jamais). Ou alors de passer par un VPN.

## Comment lancer le script

Cloner ce répo git, si vous ne savez pas comment faire, vous pouvez télécharger le zip du
répo : https://gitlab.com/resell2/vintedextract/-/archive/main/vintedextract-main.zip

```bash
python3 -m venv path/to/venv
source path/to/venv/bin/activate
pip install -r requirements.txt

python3 scrap.py <NUMERO_PUBLIQUE_DE_COMPTE_VINTED>
```

Remplacez NUMERO_PUBLIQUE_DE_COMPTE_VINTED par le numéro public du compte Vinted dont vous voulez extraire les annonces.
Vous pouvez le trouver dans l'URL du compte, par exemple pour le compte https://www.vinted.fr/member/12345678

Répétez la derniere ligne pour autant de comptes que vous voulez extraire.

A présent vous avez sauvegardé toutes vos annonces (on sait jamais, si Vinted supprime votre compte). Ou si vous voulez
créer vos annonces sur d'autres plateformes comme :

1. **LeBonCoin** - Très utilisé en France pour toutes sortes de transactions entre particuliers, y compris les
   vêtements.
2. **Vestiaire Collective** - Spécialisée dans la revente de vêtements et accessoires de luxe de seconde main.
3. **Kiabi Seconde Main** - Plateforme de Kiabi dédiée à l'achat et la vente de vêtements d'occasion.
4. **Facebook Marketplace** - Une section de Facebook où les utilisateurs peuvent acheter et vendre des articles
   localement, y compris des vêtements.
5. **eBay** - Un site d'enchères en ligne où on peut aussi vendre des vêtements de seconde main à prix fixe.
6. **Rakuten** - Plateforme de commerce électronique offrant aussi la possibilité de vendre des vêtements d'occasion.
7. **Depop** - Appréciée des jeunes adultes pour sa facilité d'utilisation et son orientation vers la mode vintage et
   faite main.
8. **Grailed** - Spécialisée dans les vêtements pour hommes, surtout les marques de designer et les pièces de
   streetwear.
9. **ThreadUp** - Un site américain de consignation en ligne pour les vêtements de femmes et d'enfants.
10. **Poshmark** - Un marché en ligne pour acheter et vendre des vêtements de mode d'occasion.

## Pré-requis

* Envrionnement Linux (pas testé sous Windows, mais ça devrait fonctionner)
    * N'hésitez pa sà me faire un retour si vous testez sous Windows, MacOS... à xc6swp3w@duck.com
* Avoir Google Chrome installé
* Environnement Python 3.8+

## A venir

* Tout sera dans Docker (avec un Dockerfile, docker-compose, etc.), pour le moment Selenium en mode headless ne
  fonctionne pas dans Docker

## Pour les estimations

Visitez ce projet là : https://resell2.gitlab.io/estim